import React, { Component } from "react";
import EventsStyle from "./EventsStyle";
import FormEvent from "./FormEvents";
import { Row, Container, Col, Card, Table } from "react-bootstrap";

// import { Container } from './styles';

export default class ListEvents extends Component {
  state = {
    fecha: "",
    hora: "",
    titulo: "",
    descricion: "",
    cantidadInvitados: "",
    localizacionGMaps: "",
    eventoRegistrer: [
      {
        fecha: "12/08/19",
        hora: "15:30",
        titulo: "Casamento Efren",
        descricion: "Casamento efren y roxana",
        cantidadInvitados: "120",
        localizacionGMaps:
          '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3902.3672036800026!2d-77.09681508543828!3d-12.018221844641717!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9105ce9908336d49%3A0x168fb8dd16a7e7b!2sSal%C3%B3n%20de%20recepciones%20el%20Dorado%20del%20Olivar!5e0!3m2!1spt-BR!2spe!4v1567389231540!5m2!1spt-BR!2spe" width="200" height="100" frameborder="0" style="border:0;" allowfullscreen=""></iframe>'
      },
      {
        fecha: "24/12/19",
        hora: "15:30",
        titulo: "Aniversario Junior",
        descricion: "Aniversario de Sebastiao Junior",
        cantidadInvitados: "50",
        localizacionGMaps:
          '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d240097.495094325!2d-44.104137155779284!3d-19.902338639378392!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xa690cacacf2c33%3A0x5b35795e3ad23997!2sBelo%20Horizonte%2C%20MG%2C%20Brasil!5e0!3m2!1spt-BR!2spe!4v1567389357224!5m2!1spt-BR!2spe" width="400" height="300" frameborder="0" style="border:0;" allowfullscreen=""></iframe>'
      }
    ]
  };

  deletHandler = i => {
    const eventoRegistrer = [...this.state.eventoRegistrer];
    eventoRegistrer.splice(i, 1);
    this.setState({
      eventoRegistrer: eventoRegistrer
    });
  };

  changeHandler = e => {
    const { name, value } = e.target;
    this.setState({
      [name]: value
    });
    console.log(this.state);
  };
  submitDobleHandler = i => {
    const eventoRegistrer = [this.state.eventoRegistrer];
    eventoRegistrer.push(eventoRegistrer, 1);
    this.setState({
      eventoRegistrer: eventoRegistrer
    });
    console.log(this.state);
  };
  submitHandler = () => {
    const eventos = {
      fecha: this.state.fecha,
      hora: this.state.hora,
      titulo: this.state.titulo,
      descricion: this.state.descricion,
      cantidadInvitados: this.state.cantidadInvitados,
      localizacionGMaps: this.state.localizacionGMaps
    };
    const eventoRegistrer = [...this.state.eventoRegistrer];

    eventoRegistrer.push(eventos);

    this.setState({
      eventoRegistrer: eventoRegistrer,
      fecha: "",
      hora: "",
      titulo: "",
      descricion: "",
      cantidadInvitados: "",
      localizacionGMaps: ""
    });
  };

  render() {
    return (
      <div>
        {this.state.eventoRegistrer.map((item, i) => (
          <EventsStyle
            key={i}
            item={item}
            deletHandler={this.deletHandler}
            submitDobleHandler={this.submitDobleHandler}
            eventoRegistrer={[...this.state.eventoRegistrer]}
          />
        ))}
        <Container>
          <Row>
            <Col xs={4}>
              <FormEvent
                changeHandler={e => this.changeHandler(e)}
                submitHandler={this.submitHandler}
                fecha={this.state.fecha}
                hora={this.state.hora}
                titulo={this.state.titulo}
                descricion={this.state.descricion}
                localizacionGMaps={this.state.localizacionGMaps}
                cantidadInvitados={this.state.cantidadInvitados}
              />
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}
