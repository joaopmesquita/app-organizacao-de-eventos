import React from "react";
import { Button } from "react-bootstrap";

// import { Container } from './styles';

const EventsStyle = props => {
  return (
    <div>
      <p>{props.item.fecha}</p>
      <p>{props.item.hora}</p>
      <p>{props.item.titulo}</p>
      <p>{props.item.descricion}</p>
      <p>{props.item.cantidadInvitados}</p>
      <p>{props.item.localizacionGMaps}</p>
      <Button onClick={props.deletHandler}>Delete</Button>
      <Button onClick={props.submitDobleHandler}>Duplicar</Button>
    </div>
  );
};

export default EventsStyle;
