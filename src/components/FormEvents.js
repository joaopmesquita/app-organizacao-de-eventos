import React from "react";
import { Form, Button, Image } from "react-bootstrap";

const FormEvents = props => {
  return (
    <div className="Formadd">
      <Form>
        <Form.Group>
          <Form.Label>titulo</Form.Label>
          <Form.Control
            type="date"
            placeholder="19/09/2019"
            onChange={props.changeHandler}
            name="fecha"
            value={props.fecha}
          />
          <Form.Text className="text-muted"></Form.Text>
        </Form.Group>
        <Form.Group>
          <Form.Label>titulo</Form.Label>
          <Form.Control
            type="text"
            placeholder="Adicione um titulo"
            onChange={props.changeHandler}
            name="titulo"
            value={props.titulo}
          />
          <Form.Text className="text-muted"></Form.Text>
        </Form.Group>

        <Form.Group>
          <Form.Label>Descripción</Form.Label>
          <Form.Control
            type="text"
            onChange={props.changeHandler}
            value={props.descricion}
            name="descricion"
          />
          <Form.Text className="text-muted"></Form.Text>
        </Form.Group>

        <Form.Group>
          <Form.Label>Cantidad de Invitados</Form.Label>
          <Form.Control
            type="Text"
            onChange={props.changeHandler}
            value={props.cantidadInvitados}
            name="cantidadInvitados"
          />
        </Form.Group>

        <Form.Group>
          <Form.Label>localizacionGMaps</Form.Label>
          <Form.Control
            type="Text"
            onChange={props.changeHandler}
            value={props.localizacionGMaps}
            name="localizacionGMaps"
          />
        </Form.Group>

        <Button type="button" onClick={props.submitHandler}>
          enviar
        </Button>
      </Form>
    </div>
  );
};

export default FormEvents;
