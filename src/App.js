import React from "react";
import ListEvents from "./components/ListEvent";

function App() {
  return (
    <div>
      <ListEvents />
    </div>
  );
}

export default App;
